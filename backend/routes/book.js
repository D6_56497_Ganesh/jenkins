const express =require('express')

const db=require('../db')
const utils=require('../utils')

const router=express.Router()

router.get('/',(request,response)=>{
    const query=`select * from book `

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.post('/',(request,response)=>{
    const {book_title, publisher_name, author_name}=request.body
    const query=`insert into book values (default,'${book_title}','${publisher_name}','${author_name}') `

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id',(request,response)=>{
    const { publisher_name, author_name}=request.body
    const query=`update  book set publisher_name='${publisher_name}',
    author_name='${author_name}'
    where book_id='${id}'
     `

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:id',(request,response)=>{
    const { id}=request.body
    const query=`delete from book
    where book_id='${id}'
     `

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports=router